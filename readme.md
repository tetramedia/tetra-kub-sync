Simple script for sync **local directory** with a remote directory of current alive kubernetes `Pod`.

For developer purpose only - when you need test something in real kubernetes environment.

Work without any server component, just one way sync - from developer computer to `Pod`.

## Install example

```shell script
curl -s "https://gitlab.com/tetramedia/tetra-kub-sync/-/raw/master/tetra-kub-sync.sh" -o /bin/tetra-kub-sync && chmod 777 /bin/tetra-kub-sync
```

## Usage example

```shell script
tetra-kub-sync -l /var/best/node-project/ -p default/node-project-0 -r /var/best/node-project/
```

Don't forget change `entrypoint` of your software. For example add to yaml-file:

```yaml
        command: [ "/bin/sh", "-c", "--" ]
        args: [ "while true; do sleep 30; done;" ]
```

And rerun your code with `kubectl` when you need. For example:

```shell script
kubectl exec --namespace="default" -it node-project-0 -- sh -c "node /var/best/node-project/server.js"
```

## Params

```shell script
Params:
-l: local directory path
-r: remote in Kubernetes pod directory path
-p: pod identifier in format "namespace/podName"
-d: enable debug mode
```
