#!/bin/bash

################################################################
# COLORS

COLOR_RED='\033[0;31m'
COLOR_GREEN='\033[0;32m'
COLOR_BLUE='\033[0;34m'
COLOR_YELLOW='\033[1;33m'
COLOR_NONE='\033[0m'


################################################################
# Functions

function do_kubectl_cmd () {

  i=0;
  while [ $i -lt 20 ] ;
  do
#    KUBECTL_CMD=$(bash -c "$1" 2>&1) ;
    KUBECTL_CMD=$(eval "$1" 2>&1) ;
    KUBECTL_CMD_EXIT_CODE=$?
    i=$((i+1));

    if [ $KUBECTL_CMD_EXIT_CODE != 0 ]; then
      if ! echo "$KUBECTL_CMD" | grep -qi "rpc\|container not found"; then
        echo "$KUBECTL_CMD"
        return "$KUBECTL_CMD_EXIT_CODE"
      fi
    else
      echo "$KUBECTL_CMD"
      return "$KUBECTL_CMD_EXIT_CODE"
    fi

  done;

exit 1

}


function print_usage() {
  echo ""
  echo "Params:"
  echo "-l: local directory path"
  echo "-r: remote in Kubernetes pod directory path"
  echo "-p: pod identifier in format \"namespace/podName\""
  echo "-d: enable debug mode"
  echo ""
  echo "Example: tetra-kub-sync -l /var/best/node-project/ -p default/node-project-0 -r /var/best/node-project/"
  echo ""
}


################################################################
# Test - is inotifywait installed ?

if ! command -v inotifywait &> /dev/null
then
    echo "${COLOR_RED}inotifywait - not installed${COLOR_NONE}"
    # Check - if OS = Debian or Ubuntu - install
    if cat /etc/os-release |  grep -qi "Debian\|Ubuntu"; then
      echo "OS = Ubuntu/Debian"
      echo "Try install inotifywait"
      apt-get update && apt-get install -y inotify-tools

      if ! command -v inotifywait &> /dev/null
      then
        echo "Error installing inotifywait - Please install inotifywait"
        exit 1;
      else
        echo "inotifywait installed successfully!"
      fi
    else
      echo "Please install inotifywait"
      exit 1;
    fi
fi

################################################################
# Test - is kubectl installed ?

if ! command -v kubectl &> /dev/null
then
    echo "${COLOR_RED}kubectl - not installed${COLOR_NONE}"

    # Check - if OS = Debian or Ubuntu - install
    if cat /etc/os-release |  grep -qi "Debian\|Ubuntu"; then
        echo "OS = Ubuntu/Debian"
        echo "Try install kubectl"

        # https://kubernetes.io/docs/tasks/tools/install-kubectl/
        apt-get update && apt-get install -y apt-transport-https gnupg2 curl inotify-tools
        curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
        echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
        apt-get update
        apt-get install -y kubectl

        if ! command -v kubectl &> /dev/null
        then
          echo "Error installing kubectl - visit https://kubernetes.io/docs/tasks/tools/install-kubectl/"
          exit 1;
        else
          echo "kubectl installed successfully!"
        fi
    else
      echo "Please install kubectl - https://kubernetes.io/docs/tasks/tools/install-kubectl/"
      exit 1;
    fi
fi

DEBUG_MODE=false

while getopts 'l:r:p:d' flag; do
  case "${flag}" in
    l) PATH_LOCAL="${OPTARG}" ;;
    r) PATH_REMOTE="${OPTARG}" ;;
    p) POD_IDENTIFIER="${OPTARG}" ;;
    d) DEBUG_MODE=true ;;
    *) print_usage
       exit 1 ;;
  esac
done

# Check: are all parameters set?
if [ -z ${PATH_LOCAL+x} ] || [ -z ${PATH_REMOTE+x} ] || [ -z ${POD_IDENTIFIER+x} ]; then echo "Error parameters..." ; print_usage ; exit 1; fi

POD_NAMESPACE=$(echo $POD_IDENTIFIER | awk -F/ '{ print $1 ; }');
POD_NAME=$(echo $POD_IDENTIFIER | awk -F/ '{ print $2 ; }');

if [ "$POD_NAMESPACE" == "" ];then
    echo "Error in pod identifier..." ;
    print_usage ;
    exit 1;
fi

if [ "$POD_NAME" == "" ];then
   POD_NAME=$POD_NAMESPACE
   POD_NAMESPACE="default"
fi

if [ "${PATH_LOCAL: -1}" != "/" ]; then
  PATH_LOCAL=$PATH_LOCAL/
fi

if [ "${PATH_REMOTE: -1}" != "/" ]; then
  PATH_REMOTE=$PATH_REMOTE/
fi


if $DEBUG_MODE ; then
  echo "====================================="
  echo "CONFIG"
  echo "====================================="
  echo "PATH_LOCAL=$PATH_LOCAL"
  echo "PATH_REMOTE=$PATH_REMOTE"
  echo "POD_IDENTIFIER=$POD_IDENTIFIER"
  echo "POD_NAMESPACE=$POD_NAMESPACE"
  echo "POD_NAME=$POD_NAME"
  echo "====================================="
fi

TEST_POD=$(kubectl get pod $POD_NAME --namespace=$POD_NAMESPACE | grep $POD_NAME | awk '{ print $1; }')

if [ "$TEST_POD" != "$POD_NAME" ]; then
  echo "ERROR!"
  printf "${COLOR_RED}Can't find pod \"$POD_NAME\" from namespace \"$POD_NAMESPACE\" on configured cluster${COLOR_NONE}\n"
  echo "Exit..."
  exit 1;
fi

printf "${COLOR_GREEN}"
printf "  _______ ______ _______ _____                 _  ___    _ ____        _______     ___   _  _____ \n"
printf " |__   __|  ____|__   __|  __ \     /\        | |/ / |  | |  _ \      / ____\ \   / / \ | |/ ____|\n"
printf "    | |  | |__     | |  | |__) |   /  \ ______| ' /| |  | | |_) |____| (___  \ \_/ /|  \| | |     \n"
printf "    | |  |  __|    | |  |  _  /   / /\ \______|  < | |  | |  _ <______\___ \  \   / | . \` | |     \n"
printf "    | |  | |____   | |  | | \ \  / ____ \     | . \| |__| | |_) |     ____) |  | |  | |\  | |____ \n"
printf "    |_|  |______|  |_|  |_|  \_\/_/    \_\    |_|\_\\____/|____/     |_____/   |_|  |_| \_|\_____|\n"

printf "${COLOR_NONE}\n"

# printf "${COLOR_GREEN}Ok! Pod \"$POD_NAME\" from namespace \"$POD_NAMESPACE\" already exist!${COLOR_NONE}\n"

echo ""
echo "Start watching for changes in local directory \"$PATH_LOCAL\" ..."

inotifywait -m -r $PATH_LOCAL -e create -e moved_to --exclude ".*(\.git|\.idea)" |
    while read path action file; do

      CHANGED_FILE=$file
      CHANGED_PATH=$path
      CHANGED_SHORT_PATH="${path/$PATH_LOCAL/""}"
      if [ "${CHANGED_FILE: -1}" == "~" ]; then
        CHANGED_FILE=${CHANGED_FILE%?}
      fi

      CHANGED_FILE_PATH=$CHANGED_PATH$CHANGED_FILE
      REMOTE_FILE_PATH=$PATH_REMOTE$CHANGED_SHORT_PATH$CHANGED_FILE

      printf  "$(date '+%Y-%m-%d %H:%M:%S') - $CHANGED_SHORT_PATH$CHANGED_FILE - "

      if $DEBUG_MODE ; then
        echo "CHANGED_FILE=$CHANGED_FILE"
        echo "CHANGED_SHORT_PATH=$CHANGED_SHORT_PATH"
        echo "REMOTE_FILE_PATH=$REMOTE_FILE_PATH"
      fi

#      COPY_COMMAND=$(kubectl cp $CHANGED_FILE_PATH $POD_NAMESPACE/$POD_NAME:$REMOTE_FILE_PATH 2>&1);
      COPY_COMMAND=$(do_kubectl_cmd "kubectl cp $CHANGED_FILE_PATH $POD_NAMESPACE/$POD_NAME:$REMOTE_FILE_PATH");

	    if $COPY_COMMAND; then
	      printf  "${COLOR_GREEN}synced${COLOR_NONE}\n"
      else
        if echo $COPY_COMMAND |  grep -qi "No such file or directory"; then
          printf "${COLOR_YELLOW}$PATH_REMOTE$CHANGED_SHORT_PATH = No such directory\n"
          printf "Try make directory $PATH_REMOTE$CHANGED_SHORT_PATH\n${COLOR_NONE}"
#          kubectl exec --namespace="$POD_NAMESPACE" -it $POD_NAME  -- sh -c "mkdir -m 777 -p $PATH_REMOTE$CHANGED_SHORT_PATH"
          do_kubectl_cmd "kubectl exec --namespace=\"$POD_NAMESPACE\" -it $POD_NAME  -- sh -c \"mkdir -m 777 -p $PATH_REMOTE$CHANGED_SHORT_PATH\""
          if do_kubectl_cmd "kubectl cp $CHANGED_FILE_PATH $POD_NAMESPACE/$POD_NAME:$REMOTE_FILE_PATH"; then
            printf  "${COLOR_GREEN}synced${COLOR_NONE}\n"
          else
            printf "${COLOR_RED}ERROR!${COLOR_NONE}\n"
          fi
        else
          printf "${COLOR_RED}ERROR!${COLOR_NONE}\n"
        fi
      fi

    done